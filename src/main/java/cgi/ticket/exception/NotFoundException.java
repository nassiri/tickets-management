package cgi.ticket.exception;

public class NotFoundException extends Exception{

	private static final long serialVersionUID = -3372433568932641320L;
	
	private String errorMessage;
	
	public String getErrorMessage() {
		return errorMessage;
	}

	public NotFoundException() {
		super();
	}
	
	public NotFoundException(String errorMessage) {
		super(errorMessage);
		this.errorMessage = errorMessage;
	}

}
