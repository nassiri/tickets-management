package cgi.ticket.reporting;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.multipart.MultipartFile;

import cgi.ticket.entity.Ticket;
import cgi.ticket.repository.TicketRepository;
import cgi.ticket.utils.Priorite;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TicketReportCSV {
	
	@Autowired
	private static TicketRepository ticketRepository;

	public static final String SUCESS_MESSAGE = "***** le fichier est uploder avec success *****";
	public static final String FAILED_MESSAGE = "***** Reading CSV Error  ******";

	public static void downloadTicketCSV(PrintWriter writer, List<Ticket> tickets) {
		try (CSVPrinter csvPrinter = new CSVPrinter(writer,
				CSVFormat.DEFAULT.withHeader("TITRE", "DESCRIPTION", "TEMPS", "PRIORETE", "DATEOPERATION"));) {
			for (Ticket ticket : tickets) {
				List<String> data = Arrays.asList(ticket.getTitre(), ticket.getDescription(),
						ticket.getTempsPasse().toString(), ticket.getPriorete().toString(),
						ticket.getDateOperation() == null ? "2017-07-07" : ticket.getDateOperation().toString());

				csvPrinter.printRecord(data);
			}
			csvPrinter.flush();
		} catch (Exception e) {
			log.error("*********** Writing CSV error **********");
		}
	}

	public static void uploadTicketCSV(MultipartFile file) throws IOException {
		
		InputStream inputStream =  new BufferedInputStream(file.getInputStream());
		BufferedReader fileReader = null;
		CSVParser csvParser = null;

		List<Ticket> tickets = new ArrayList<Ticket>();

		try {
			fileReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
			csvParser = new CSVParser(fileReader,
					CSVFormat.DEFAULT.withFirstRecordAsHeader().withIgnoreHeaderCase().withTrim());

			Iterable<CSVRecord> csvRecords = csvParser.getRecords();

			for (CSVRecord csvRecord : csvRecords) {
				Ticket ticket = new Ticket((csvRecord.get("TITRE")), csvRecord.get("DESCRIPTION"),
						Integer.parseInt(csvRecord.get("TEMPS")),
						new SimpleDateFormat("yyyy-MM-dd").parse(csvRecord.get("DATEOPERATION")));

				tickets.add(ticket);
			}
			ticketRepository.saveAll(tickets);
		} catch (Exception e) {
			log.error(String.format(FAILED_MESSAGE, e.getMessage()));
		}
	}
}
