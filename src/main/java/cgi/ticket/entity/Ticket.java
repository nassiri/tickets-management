package cgi.ticket.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import cgi.ticket.audit.Auditable;
import cgi.ticket.utils.Priorite;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "TICKET")
public class Ticket extends Auditable<String> implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull(message = "ce champs est obligatoire")
	@Column(name = "TITRE")
	private String titre;

	@Lob
	@Column(name = "DESCRIPTION", length = 512)
	private String description;

	@Column(name = "NOM_FICHIER")
	private String fileName;

	@Lob
	private byte[] fileBody;

	@Lob
	@Column(name = "RESOLUTION", length = 512)
	private String resolution;

	@Column(name = "TEMPS")
	@NotNull(message = "ce champs est obligatoire")
	private Integer tempsPasse;

	@Enumerated(EnumType.STRING)
	private Priorite priorete;

	@Column(name = "Date_Operation")
	@Temporal(TemporalType.DATE)
	private Date dateOperation;

	@Column(name = "Date_Resolution")
	@Temporal(TemporalType.DATE)
	private Date dateDeResolutionEstimer;

	@Transient
	private Long userId;

	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "statut_ticket_id")
	private StatutTicket statutTicket;

	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "prestataire_id")
	private Prestataire prestataire;

	public Ticket(@NotNull(message = "ce champs est obligatoire") String titre, String description,
			@NotNull(message = "ce champs est obligatoire") Integer tempsPasse, Date dateOperation) {
		super();
		this.titre = titre;
		this.description = description;
		this.tempsPasse = tempsPasse;
		this.dateOperation = dateOperation;
	}

}
