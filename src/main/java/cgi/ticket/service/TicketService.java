package cgi.ticket.service;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

import cgi.ticket.entity.Ticket;
import cgi.ticket.exception.NotFoundException;
import cgi.ticket.exception.TechnicalException;
import cgi.ticket.utils.Priorite;

public interface TicketService {
	
	List<Ticket> afficherTickets() throws TechnicalException;
	
	Page<Ticket> afficherTicketsAvecOrder(int page,int size) throws TechnicalException;
	
	Optional<Ticket> afficherTicketParId(Long id) throws NotFoundException;
	
	Optional<Ticket> afficherTicketParTitreEtPriorite(String nom,Priorite priorite) throws NotFoundException;
	
	Ticket saveTicket(Ticket ticket) throws NotFoundException;
	
	Ticket modifierTicket(Ticket ticket,Long id) throws NotFoundException;
	
	Boolean deleteTicket(Long id) throws NotFoundException;
	
	Optional<Ticket> downloadFichier(Long id) throws NotFoundException;
	
	Boolean uploadFichier(Long id,MultipartFile file) throws NotFoundException, IOException;
	
	String uploadImages(MultipartFile[] files) throws IOException;
	
	byte[] downloadImages(String name) throws NotFoundException, IOException;
}
