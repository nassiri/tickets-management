package cgi.ticket.service;

import static cgi.ticket.utils.MessageLogs.ERROR_FUNCTIONAL_RECOVER;
import static cgi.ticket.utils.MessageLogs.OBJECT_NOT_FOUND;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import cgi.ticket.entity.Ticket;
import cgi.ticket.exception.NotFoundException;
import cgi.ticket.exception.TechnicalException;
import cgi.ticket.repository.TicketRepository;
import cgi.ticket.utils.Priorite;

@Service
@Transactional
public class TicketServiceImp implements TicketService {

	@Autowired
	private TicketRepository ticketRepository;

	@Value("classpath:src/main/resources/images/")
	Resource resourceFile;

	public static final String SUCCESS_MESSAGE = "**** le fichier est enregidter cote server avec success ****";

	@Override
	public Optional<Ticket> afficherTicketParId(Long id) throws NotFoundException {
		Optional<Ticket> ticket = ticketRepository.findById(id);
		if (!ticket.isPresent())
			throw new NotFoundException("l'identifiant " + id + " est introvable");
		return ticket;
	}

	@Override
	public Ticket saveTicket(@Valid Ticket ticket) throws NotFoundException {
		if (ticket == null)
			throw new NotFoundException("le ticket " + ticket + " ne doit pas etre null");
		return ticketRepository.save(ticket);
	}

	@Override
	public Boolean deleteTicket(Long id) throws NotFoundException {
		if (id.equals(null))
			throw new NotFoundException("l'identifiant " + id + " ne doit pas etre null");
		Ticket ticket = ticketRepository.findById(id)
				.orElseThrow(() -> new NotFoundException("le ticket avec l'identifiant " + id + " est intouvable"));
		ticketRepository.delete(ticket);
		return true;

	}

	@Override
	public Ticket modifierTicket(Ticket ticket, Long id) throws NotFoundException {
		if (id == null && ticket == null)
			throw new NotFoundException("l'identifiant " + id + " ne doit pas etre null");
		Optional<Ticket> ticketToUpdate = ticketRepository.findById(id);
		if (!ticketToUpdate.isPresent())
			throw new NotFoundException("le ticket " + ticketToUpdate.get() + " est intouvable");
		ticketToUpdate.get().setDateDeResolutionEstimer(ticket.getDateDeResolutionEstimer());
		ticketToUpdate.get().setTitre(ticket.getTitre());
		ticketToUpdate.get().setPriorete(ticket.getPriorete());
		return ticketRepository.saveAndFlush(ticketToUpdate.get());
	}

	@Override
	public Page<Ticket> afficherTicketsAvecOrder(int page, int size) throws TechnicalException {
		Page<Ticket> tickets = ticketRepository.findAllByOrderByDateOperationAsc(PageRequest.of(page, size));
		if (tickets.isEmpty())
			throw new TechnicalException("la list des tickets est vide");
		return tickets;
	}

	@Override
	public Optional<Ticket> afficherTicketParTitreEtPriorite(String titre, Priorite priorite) throws NotFoundException {
		if (titre.equals(null) && priorite.equals(null))
			throw new NotFoundException("veuillez remplir les chams de la recherche");
		Optional<Ticket> ticket = ticketRepository.findByTitreAndPriorete(titre, priorite);
		if (!ticket.isPresent())
			throw new NotFoundException("le ticket est introuvable");
		return ticket;
	}

	@Override
	public List<Ticket> afficherTickets() throws TechnicalException {
		List<Ticket> tickets = ticketRepository.findAll();
		if (tickets.isEmpty())
			throw new TechnicalException("la list des tickets est vide");
		return tickets;
	}

	@Override
	public Optional<Ticket> downloadFichier(Long id) throws NotFoundException {
		if (id == null)
			throw new NotFoundException(String.format(OBJECT_NOT_FOUND, id));
		Optional<Ticket> ticket = ticketRepository.findById(id);
		if (!ticket.isPresent())
			throw new NotFoundException(String.format(ERROR_FUNCTIONAL_RECOVER, ticket.get()));
		return ticket;
	}

	@Override
	public Boolean uploadFichier(Long id, MultipartFile file) throws NotFoundException, IOException {
		if (id == null && file == null)
			throw new NotFoundException(String.format(OBJECT_NOT_FOUND, id));
		Optional<Ticket> ticket = ticketRepository.findById(id);
		if (!ticket.isPresent())
			throw new NotFoundException(String.format(ERROR_FUNCTIONAL_RECOVER, ticket.get()));
		ticket.get().setFileName(file.getOriginalFilename());
		ticket.get().setFileBody(file.getBytes());
		ticketRepository.save(ticket.get());
		return true;
	}

	@Override
	public String uploadImages(MultipartFile[] files) throws IOException {
		for (MultipartFile file : files) {
			Path pathImage = Paths.get("src/main/resources/images/", file.getOriginalFilename());
			byte[] bytesImage = file.getBytes();
			try {
				Files.write(pathImage, bytesImage);
			} catch (Exception e) {
				throw new IOException(String.format(ERROR_FUNCTIONAL_RECOVER, e));
			}
		}
		return SUCCESS_MESSAGE;
	}

	@Override
	public byte[] downloadImages(String name) throws NotFoundException, IOException {
		if (name.equals(null))
			throw new NotFoundException("le fichier " + name + " n'existe pas");
		Resource resource = new ClassPathResource("images/" + name);
		try {
			File file = resource.getFile();
			byte[] fileContent = Files.readAllBytes(file.toPath());
			return fileContent;
		} catch (Exception e) {
			throw new IOException(e.getMessage());
		}
	}

}
