package cgi.ticket.config;

import java.util.Arrays;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Aspect
@Component
public class LogsAcquisition {
	final String execution = "execution(* cgi.ticket.service.*.*(..))";

	@Before(execution)
	public void logBefore(JoinPoint objJoinpoint) {
		log.info("******* Start method: " + objJoinpoint.getSignature().getName() + " *******");
	}

	@After(execution)
	public void logAfter(JoinPoint objJoinpoint) {
		 log.debug("******* END method: " +
		 objJoinpoint.getSignature().getName() + " *******");
	}

	@AfterReturning(pointcut = execution, returning = "result")
	public void logAfterReturning(JoinPoint objJoinpoint, Object result) {
		log.info("******* Arguments : " + Arrays.toString(objJoinpoint.getArgs()) + " *******");
		log.info("******* Method returned value is : " + result);
		log.info("******* END method: " + objJoinpoint.getSignature().getName() + " *******");
	}

	@AfterThrowing(pointcut = execution, throwing = "error")
	public void logAfterThrowing(JoinPoint objJoinpoint, Throwable error) {
		 log.info("******* Arguments : " +
		 Arrays.toString(objJoinpoint.getArgs()) + " *******");
		 log.error("******* Exception : " + error);
	}
}