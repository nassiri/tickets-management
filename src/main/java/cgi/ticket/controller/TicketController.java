package cgi.ticket.controller;

import static cgi.ticket.utils.Pagination.PAGE;
import static cgi.ticket.utils.Pagination.SIZE;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import cgi.ticket.entity.Ticket;
import cgi.ticket.exception.NotFoundException;
import cgi.ticket.exception.TechnicalException;
import cgi.ticket.reporting.TicketReportCSV;
import cgi.ticket.service.TicketServiceImp;
import cgi.ticket.utils.Priorite;

@RestController
@RequestMapping("/tickets")
public class TicketController {

	@Autowired
	private TicketServiceImp ticketServiceImp;

	@GetMapping(value = "")
	public ResponseEntity<List<Ticket>> afficherTickets() throws TechnicalException {
		List<Ticket> tickets = ticketServiceImp.afficherTickets();
		return new ResponseEntity<>(tickets, HttpStatus.OK);
	}

	@GetMapping(value = "/pages")
	public ResponseEntity<Page<Ticket>> afficherTicketsParPagination(@RequestParam(defaultValue = PAGE) int page,
			@RequestParam(defaultValue = SIZE) int size) throws TechnicalException {
		Page<Ticket> tickets = ticketServiceImp.afficherTicketsAvecOrder(page, size);
		return ResponseEntity.status(HttpStatus.OK).body(tickets);
	}

	@GetMapping(value = "/{id}")
	public ResponseEntity<Ticket> afficherTicketParId(@PathVariable Long id) throws NotFoundException {
		Optional<Ticket> tickets = ticketServiceImp.afficherTicketParId(id);
		return new ResponseEntity<>(tickets.get(), HttpStatus.OK);
	}

	@GetMapping(value = "/search")
	public ResponseEntity<Ticket> afficherTicketParNomEtStatut(@RequestParam String titre,
			@RequestParam Priorite priorite) throws NotFoundException {
		Optional<Ticket> tickets = ticketServiceImp.afficherTicketParTitreEtPriorite(titre, priorite);
		return new ResponseEntity<>(tickets.get(), HttpStatus.OK);
	}

	@PostMapping(value = "")
	public ResponseEntity<Ticket> saveTicket(@RequestBody Ticket ticket) throws NotFoundException {
		Ticket tickets = ticketServiceImp.saveTicket(ticket);
		return new ResponseEntity<>(tickets, HttpStatus.CREATED);
	}

	@DeleteMapping(value = "/{id}")
	public ResponseEntity<Boolean> deleteTicket(@PathVariable Long id) throws NotFoundException {
		ticketServiceImp.deleteTicket(id);
		return new ResponseEntity<>(true, HttpStatus.NO_CONTENT);
	}

	@PutMapping(value = "/{id}")
	public ResponseEntity<Boolean> deleteTicket(@RequestBody Ticket ticket, @PathVariable Long id)
			throws NotFoundException {
		ticketServiceImp.modifierTicket(ticket, id);
		return new ResponseEntity<>(true, HttpStatus.ACCEPTED);
	}

	@GetMapping(value = "/download/{id}")
	public ResponseEntity<byte[]> telechargerFichier(@PathVariable Long id) throws NotFoundException {
		Optional<Ticket> file = ticketServiceImp.downloadFichier(id);
		return ResponseEntity.status(HttpStatus.OK)
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.get().getFileName() + "\"")
				.body(file.get().getFileBody());
	}

	@PostMapping(value = "/upload/{id}")
	public ResponseEntity<Boolean> uploadFichierPiece(@PathVariable Long id, @RequestParam("file") MultipartFile file)
			throws NotFoundException, IOException {
		return ResponseEntity.status(HttpStatus.CREATED).body(ticketServiceImp.uploadFichier(id, file));
	}

	@GetMapping(value = "/report/tickets.csv")
	public void downloadCSV(HttpServletResponse response) throws IOException, TechnicalException {
		response.setContentType("text/csv");
		response.setHeader("Content-Disposition", "attachment; file=tickets.csv");
		List<Ticket> tickets = ticketServiceImp.afficherTickets();
		TicketReportCSV.downloadTicketCSV(response.getWriter(), tickets);
	}

	@PostMapping(value = "/report/upload")
	public ResponseEntity<String> uploadTicketCSV(@RequestParam("file") MultipartFile file)
			throws NotFoundException, IOException {
		TicketReportCSV.uploadTicketCSV(file);
		return ResponseEntity.status(HttpStatus.CREATED).body(TicketReportCSV.SUCESS_MESSAGE);
	}

	@PostMapping(value = "/images/upload")
	public ResponseEntity<String> uploadImages(@RequestParam("file") MultipartFile[] files) throws IOException {
		return ResponseEntity.status(HttpStatus.ACCEPTED).body(ticketServiceImp.uploadImages(files));
	}

	@GetMapping(value = "/images/{name}/download")
	public ResponseEntity<byte[]> telechargerImages(@PathVariable String name) throws NotFoundException, IOException {
		byte[] file = ticketServiceImp.downloadImages(name);
		return ResponseEntity.status(HttpStatus.OK)
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + name + "\"")
				.body(file);
	}
}
