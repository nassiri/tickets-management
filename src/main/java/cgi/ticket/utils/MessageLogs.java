package cgi.ticket.utils;

public class MessageLogs {
	public static final String INPUT_MESSAGE = "Input => %s: {}";
	public static final String OUTPUT_MESSAGE = "Output => %s: {}";
	public static final String SHOW_LIST_ENTITIES = "Afficher la list des %s avec son Identifiant : {}";
	public static final String OBJECT_NOT_FOUND = "la %s avec l'ID = %d est introuvable";
	public static final String ERROR_FUNCTIONAL_RECOVER = "Une Erreur Fonctionnelle s'est produite lors de la recuperation de %s";
	public static final String ERROR_FUNCTIONAL_CREATE = "Une Erreur Fonctionnelle s'est produite lors de la creation de %s";
	public static final String ERROR_FUNCTIONAL_UPDATE = "Une Erreur Fonctionnelle s'est produite lors de la modification de %s";
}
