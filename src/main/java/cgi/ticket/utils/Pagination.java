package cgi.ticket.utils;

import java.io.Serializable;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class Pagination implements Serializable{
	public static final String PAGE = "0";
	public static final String SIZE = "10";
}
