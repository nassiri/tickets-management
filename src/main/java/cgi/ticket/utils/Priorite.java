package cgi.ticket.utils;

import lombok.Getter;

@Getter
public enum Priorite {
	URGENT("urgent"), IMMEDIATE("immediate"), SECONDAIRE("secondaire"), NO_URGENT("no urgent");

	private final String libelle;

	Priorite(String libelle) {
		this.libelle = libelle;
	}
}
