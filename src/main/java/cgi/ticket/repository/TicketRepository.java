package cgi.ticket.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import cgi.ticket.entity.Ticket;
import cgi.ticket.utils.Priorite;

public interface TicketRepository extends JpaRepository<Ticket, Long>, JpaSpecificationExecutor<Ticket> {

	public Optional<Ticket> findByTitreAndPriorete(String titre,Priorite priorite);
	
	public Page<Ticket> findAll(Pageable pageable);
	
	public Page<Ticket> findAllByOrderByDateOperationAsc(Pageable pageable);
	
	@Query("SELECT ticket FROM Ticket ticket WHERE ticket.statutTicket.designation ='OPEN' ORDER BY ticket.dateOperation ASC")
	public List<Ticket> getAllTicketsOrder();
}
